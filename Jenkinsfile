def label = "worker-${UUID.randomUUID().toString()}"
def DOCKER_REPO = "localhost:32002"

podTemplate(label: label, containers: [
  containerTemplate(name: 'docker', image: 'docker', command: 'cat', ttyEnabled: true),
  containerTemplate(name: 'nodejs', image: 'node:lts', command: 'cat', ttyEnabled: true),
],
volumes: [
  hostPathVolume(mountPath: '/var/run/docker.sock', hostPath: '/var/run/docker.sock'),
  persistentVolumeClaim(mountPath: '/root/owasp-cve-db', claimName: 'jenkins-demo', readOnly: false)
]) {
  node(label) {

    sh "env"

    container("nodejs") {

      env.JAVA_HOME="${tool 'default-jdk'}"
      echo "${env.JAVA_HOME}"
      env.PATH="${env.JAVA_HOME}/bin:${env.PATH}"
      sh "export PATH=${env.JAVA_HOME}/bin:${env.PATH}"
      sh "java -version"

      def myRepo = checkout scm

      stage("Install NPM dependencies") {
        sh "npm ci --quiet"
      }
      stage("Lint") {
        sh "npm run checkstyle"
      }
      stage("Test") {
        try {
          sh "npm run test"
          sh "npm run test-ci"
          sh "npm run test-cov"
        } catch (e) {
          throw e;
        } finally {
          publishHTML(target: [
            allowMissing         : false,
            alwaysLinkToLastBuild: true,
            keepAll              : true,
            reportDir            : 'reports',
            reportFiles          : 'coverage/lcov-report/index.html',
            reportName           : "Unit Tests"])
        }
      }

      stage("Third-Party Dependency Check") {
        def dependencyCheck = tool 'dependency-check'
        sh "${dependencyCheck}/bin/dependency-check.sh --project codeveros-catalog-service --scan package-lock.json --format ALL --out . --data /root/owasp-cve-db"
        archiveArtifacts allowEmptyArchive: true, artifacts: 'dependency-check-report.*', onlyIfSuccessful: false
        dependencyCheckPublisher pattern: 'dependency-check-report.xml'
      }

      stage("SonarQube") {
        def scannerHome = tool 'sonarqube-scanner';
        withSonarQubeEnv('sonarqube') {
          sh "${scannerHome}/bin/sonar-scanner \
            -Dsonar.projectKey=codeveros-catalog-service \
            -Dsonar.sources=lib,app.js -Dsonar.tests=test -Dsonar.language=js \
            -Dsonar.dependencyCheck.htmlReportPath=dependency-check-report.html \
            -Dsonar.dependencyCheck.reportPath=dependency-check-report.xml \
            -Dsonar.javascript.lcov.reportPaths=reports/coverage/lcov.info \
            -Dsonar.dependencyCheck.severity.blocker=-1 \
            -Dsonar.dependencyCheck.severity.critical=-1 \
            -Dsonar.dependencyCheck.severity.major=-1 \
            -Dsonar.dependencyCheck.severity.minor=-1 \
            -Dsonar.dependencyCheck.summarize=true"
        }
      }
    }

    stage('Build catalog service') {
      container("docker") {
        docker.withRegistry("http://${DOCKER_REPO}", 'nexus') {
          echo "building docker image for catalog service"
          def catalogImage = docker.build("${DOCKER_REPO}/catalogservice/catalogservice:latest")
          appImage = docker.build("${DOCKER_REPO}/catalogservice/catalogservice:latest")
          appImage.push()
        }
      }
    }

    // stage('trigger deployment') {
    //     build 'deployment'
    // }

  }
}
